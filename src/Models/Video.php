<?php

namespace Src\Models;

use Src\Core\Model;

class Video extends Model
{
    public function __construct()
    {
        parent::__construct("videos");
    }

    public function getById($id, $columns = ["*"])
    {
        $video = $this->findById($id, $columns);

        if ($video) {
            return $video;
        }
        return null;
    }

    public function getByLesson($lesson_id)
    {
        $video = $this->read(false, ["*"], ["lesson_id" => $lesson_id]);
        if ($video) {
            $video->type = "v";
            return $video;
        }
        return null;
    }
}

?>