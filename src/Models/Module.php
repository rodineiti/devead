<?php

namespace Src\Models;

use Src\Core\Model;

class Module extends Model
{
    public function __construct()
    {
        parent::__construct("modules");
    }

    public function getById($id, $columns = ["*"])
    {
        $course = $this->findById($id, $columns);

        if ($course) {
            return $course;
        }
        return null;
    }

    public function getByCourse($course_id)
    {
        $modules = $this->read(true, ["*"], ["course_id" => $course_id]) ?? [];

        foreach ($modules as $module) {
            $module->lessons = (new Lesson())->getByModule($module->id);
        }

        return $modules;
    }

    public function destroy($id)
    {
        $lessons = (new Lesson())->read(true, ["*"], ["module_id" => $id]);
        foreach ($lessons as $lesson) {
            $doubts = (new Doubt())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($doubts as $doubt) {
                (new Doubt())->delete(["id" => $doubt->id]);
            }
            $videos = (new Video())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($videos as $video) {
                (new Video())->delete(["id" => $video->id]);
            }
            $questions = (new Question())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($questions as $question) {
                (new Question())->delete(["id" => $question->id]);
            }
            $historics = (new Historic())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($historics as $historic) {
                (new Historic())->delete(["id" => $historic->id]);
            }
            (new Lesson())->delete(["id" => $lesson->id]);
        }
        (new Module())->delete(["course_id" => $id]);
        return $this->delete(["id" => $id]);
    }
}

?>