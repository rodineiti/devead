<?php

namespace Src\Models;

use Src\Core\Model;

class Question extends Model
{
    public function __construct()
    {
        parent::__construct("questions");
    }

    public function getById($id, $columns = ["*"])
    {
        $question = $this->findById($id, $columns);

        if ($question) {
            return $question;
        }
        return null;
    }

    public function getByLesson($lesson_id)
    {
        $question = $this->read(false, ["*"], ["lesson_id" => $lesson_id]);
        if ($question) {
            $question->type = "q";
            return $question;
        }
        return null;
    }
}

?>