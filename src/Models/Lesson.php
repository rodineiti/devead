<?php

namespace Src\Models;

use Src\Core\Model;

class Lesson extends Model
{
    public function __construct()
    {
        parent::__construct("lessons");
    }

    public function getByModule($module_id)
    {
        $this->order = "`order`";
        $lessons = $this->read(true, ["*"], ["module_id" => $module_id]) ?? [];

        foreach ($lessons as $lesson) {
            if ($lesson->type === "v") {
                $lesson->video = (new Video())->getByLesson($lesson->id);
            } else {
                $lesson->question = (new Question())->getByLesson($lesson->id);
            }
            $lesson->isMarked = (bool)(new Historic())->getMarked($lesson->id);
        }

        return $lessons;
    }

    public function getById($id, $columns = ["*"])
    {
        $lesson = $this->findById($id, $columns);

        if ($lesson) {
            if ($lesson->type === "v") {
                $lesson->video = (new Video())->getByLesson($lesson->id);
            } else {
                $lesson->question = (new Question())->getByLesson($lesson->id);
            }
            $lesson->doubts = (new Doubt())->getByLesson($lesson->id);
            $lesson->isMarked = (bool)(new Historic())->getMarked($lesson->id);
            return $lesson;
        }
        return null;
    }

    public function destroy($id)
    {
        $doubts = (new Doubt())->read(true, ["*"], ["lesson_id" => $id]);
        foreach ($doubts as $doubt) {
            (new Doubt())->delete(["id" => $doubt->id]);
        }
        $videos = (new Video())->read(true, ["*"], ["lesson_id" => $id]);
        foreach ($videos as $video) {
            (new Video())->delete(["id" => $video->id]);
        }
        $questions = (new Question())->read(true, ["*"], ["lesson_id" => $id]);
        foreach ($questions as $question) {
            (new Question())->delete(["id" => $question->id]);
        }
        $historics = (new Historic())->read(true, ["*"], ["lesson_id" => $id]);
        foreach ($historics as $historic) {
            (new Historic())->delete(["id" => $historic->id]);
        }
        return $this->delete(["id" => $id]);
    }
}

?>