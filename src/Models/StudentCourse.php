<?php

namespace Src\Models;

use Src\Core\Model;

class StudentCourse extends Model
{
    public function __construct()
    {
        parent::__construct("students_courses");
    }

    public function getByStudent($user_id)
    {
        $courses = $this->read(true, ["*"], ["user_id" => $user_id]) ?? [];

        foreach ($courses as $course) {
            $course->name = (new Course())->getById($course->course_id)->name;
        }

        return $courses;
    }
}

?>