<?php

namespace Src\Models;

use Src\Core\Model;

class Historic extends Model
{
    public function __construct()
    {
        parent::__construct("historics");
    }

    public function getById($id, $columns = ["*"])
    {
        $find = $this->findById($id, $columns);

        if ($find) {
            return $find;
        }
        return null;
    }

    public function create($lesson_id)
    {
        $data["user_id"] = auth()->id;
        $data["lesson_id"] = $lesson_id;
        $this->insert($data);
        return true;
    }

    public function getMarked($lesson_id)
    {
        if (auth()) {
            return $this->count(["id"], ["user_id" => auth()->id, "lesson_id" => $lesson_id]);
        }
        return 0;
    }
}

?>