<?php

namespace Src\Models;

use Src\Core\Model;

class User extends Model
{
    public function __construct()
    {
        parent::__construct("users");
    }

    public function all()
    {
        $users = $this->read(true, ["*"], []) ?? [];

        foreach ($users as $user) {
            $user->countCourses = (new StudentCourse())->count(["id"], ["user_id" => $user->id]) ?? [];
        }

        return $users;
    }

    public function attempt($email, $password)
    {
        $user = $this->read(false, ["*"], ["email" => $email]);

        if (!$user) {
            return false;
        }

        if (!pwd_verify($password, $user->password)) {
            return false;
        }

        if (pwd_rehash($user->password)) {
            $this->update(["password" => pwd_gen_hash($password)],  ["id" => $user->id]);
        }

        $_SESSION["userLogged"] = (object)$user;
        return true;
    }

    public function create(array $data)
    {
        if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if ($this->exists("email", $data["email"])) {
            return false;
        }

        $data["password"] = pwd_gen_hash($data["password"]);
        $user = $this->insert($data);
        return $user;
    }

    public function getById($id, $columns = ["id", "name", "email", "genre", "photo"])
    {
        $user = $this->findById($id, $columns);

        if ($user) {
            return $user;
        }
        return null;
    }

    public function updateProfile($id, array $data)
    {
        $newData = array();
        if (!empty($data["email"])) {
            $newData["email"] = $data["email"];
        }

        if (!empty($data["name"])) {
            $newData["name"] = $data["name"];
        }

        if (!empty($data["genre"])) {
            $newData["genre"] = $data["genre"];
        }

        if (!empty($data["password"])) {
            $newData["password"] = pwd_gen_hash($data["password"]);
        }

        if (isset($newData["email"]) && !filter_var($newData["email"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (isset($newData["email"]) && $this->existsAux("email", $newData["email"], $id)) {
            return false;
        }

        unset($newData["email"]);

        if (count($newData)) {
            if ($this->update($newData, ["id" => $id])) {
                $user = $this->read(false, ["*"], ["id" => $id]);
                return $user;
            }
        }

        return false;
    }

    private function exists($field, $value)
    {
        return $this->read(false, ["*"], [$field => $value]);
    }

    private function existsAux($field, $value, $id)
    {
        return $this->read(false, ["*"], [$field => $value], ["id", "NOT IN", [$id]]);
    }

    public function getCourses()
    {
        $courses = (new StudentCourse())->read(true, ["*"], ["user_id" => auth()->id]) ?? [];

        foreach ($courses as $value) {
            $course = (new Course())->getById($value->course_id);
            $value->name = $course->name;
            $value->photo = $course->photo;
            $value->description = $course->description;
        }

        return $courses;
    }

    public function isRegisteredCourse($course_id)
    {
        return (new StudentCourse())->count(["id"], ["user_id" => auth()->id, "course_id" => $course_id]) ? true : false;
    }

    public function destroy($id)
    {
        (new StudentCourse())->delete(["user_id" => $id]);
        $historics = (new Historic())->read(true, ["*"], ["user_id" => $id]);
        foreach ($historics as $historic) {
            (new Historic())->delete(["id" => $historic->id]);
        }
        $doubts = (new Doubt())->read(true, ["*"], ["user_id" => $id]);
        foreach ($doubts as $doubt) {
            (new Doubt())->delete(["id" => $doubt->id]);
        }
        return $this->delete(["id" => $id]);
    }

    public function addCourse($user_id, $course_id)
    {
        $course = (new StudentCourse())->read(false, ["*"], ["user_id" => $user_id, "course_id" => $course_id]);
        if (!$course) {
            (new StudentCourse())->insert(["user_id" => $user_id, "course_id" => $course_id]);
            return true;
        }
        return false;
    }

    public function delCourse($user_id, $course_id)
    {
        $course = (new StudentCourse())->read(false, ["*"], ["user_id" => $user_id, "course_id" => $course_id]);
        if ($course) {
            (new StudentCourse())->delete(["id" => $course->id]);
            return true;
        }
        return false;
    }

    public function getCountWatched()
    {
        return (new Historic())->count(["id"], ["user_id" => auth()->id]) ?? 0;
    }
}

?>