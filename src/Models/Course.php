<?php

namespace Src\Models;

use Src\Core\Model;

class Course extends Model
{
    public function __construct()
    {
        parent::__construct("courses");
    }

    public function getById($id, $columns = ["id", "name", "photo", "description"])
    {
        $course = $this->findById($id, $columns);

        if ($course) {
            $course->countStudents = (new StudentCourse())->count(["id"], ["course_id" => $course->id]);
            return $course;
        }
        return null;
    }

    public function all($whereIn = null)
    {
        if (!$whereIn) {
            $courses = $this->read(true, ["*"], []) ?? [];
        } else {
            $courses = $this->read(true, ["*"], [], ["id", "NOT IN", $whereIn]) ?? [];
        }

        foreach ($courses as $course) {
            $course->countStudents = (new StudentCourse())->count(["id"], ["course_id" => $course->id]) ?? [];
        }

        return $courses;
    }

    public function add(array $data)
    {
        if (isset($data["file"]) && count($data["file"])) {
            if (in_array($data["file"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $tmpname = md5(time().rand(0,9999));
                $ext  = (in_array($data["file"]["type"], ["image/jpeg", "image/jpg"]) ? ".jpg" : ".png");
                move_uploaded_file($data["file"]['tmp_name'], 'media/photos/'.$tmpname.$ext);
                $data["photo"] = $tmpname.$ext;
            }
        }

        unset($data["file"]);
        $save = $this->insert($data);

        if ($save) {
            return $this->read(false, ["*"], ["id" => $save]);
        } else {
            return null;
        }
    }

    public function updateData(array $data, $id)
    {
        if (isset($data["file"]) && count($data["file"])) {
            if (in_array($data["file"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $tmpname = md5(time().rand(0,9999));
                $ext  = (in_array($data["file"]["type"], ["image/jpeg", "image/jpg"]) ? ".jpg" : ".png");
                move_uploaded_file($data["file"]['tmp_name'], 'media/photos/'.$tmpname.$ext);
                $data["photo"] = $tmpname.$ext;
            }
        }

        unset($data["file"]);
        return $this->update($data, ["id" => $id]);
    }

    public function destroy($id)
    {
        (new StudentCourse())->delete(["course_id" => $id]);
        $lessons = (new Lesson())->read(true, ["*"], ["course_id" => $id]);
        foreach ($lessons as $lesson) {
            $doubts = (new Doubt())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($doubts as $doubt) {
                (new Doubt())->delete(["id" => $doubt->id]);
            }
            $videos = (new Video())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($videos as $video) {
                (new Video())->delete(["id" => $video->id]);
            }
            $questions = (new Question())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($questions as $question) {
                (new Question())->delete(["id" => $question->id]);
            }
            $historics = (new Historic())->read(true, ["*"], ["lesson_id" => $lesson->id]);
            foreach ($historics as $historic) {
                (new Historic())->delete(["id" => $historic->id]);
            }
            (new Lesson())->delete(["id" => $lesson->id]);
        }
        (new Module())->delete(["course_id" => $id]);
        return $this->delete(["id" => $id]);
    }

    public function getTotalLessons($course_id)
    {
        return (new Lesson())->count(["id"], ["course_id" => $course_id]) ?? 0;
    }
}

?>