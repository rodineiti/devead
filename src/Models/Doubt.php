<?php

namespace Src\Models;

use Src\Core\Model;

class Doubt extends Model
{
    public function __construct()
    {
        parent::__construct("doubts");
    }

    public function getById($id, $columns = ["*"])
    {
        $find = $this->findById($id, $columns);

        if ($find) {
            return $find;
        }
        return null;
    }

    public function getByLesson($lesson_id)
    {
        $video = $this->read(true, ["*"], ["lesson_id" => $lesson_id]);
        return $video ?? [];
    }

    public function create(array $data, $lesson_id)
    {
        $data["user_id"] = auth()->id;
        $data["lesson_id"] = $lesson_id;
        $this->insert($data);
        return true;
    }
}

?>