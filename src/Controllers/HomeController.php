<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->auth();
    }

    public function index()
    {
        $data = array();
        $data["studentCourses"] = (new User())->getCourses();
        $this->template("home", $data);
    }
}