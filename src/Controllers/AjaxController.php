<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Historic;
use Src\Models\Lesson;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->auth();
    }

    public function mark_done()
    {
        if ($this->method() !== "POST") {
            $this->json(["error" => true, "message" => "Method not allowed"]);
        }

        $lesson_id = filter_var($this->request()["lesson_id"], FILTER_VALIDATE_INT);

        if ($lesson_id) {

            $lesson = (new Lesson())->getById($lesson_id);

            if (!$lesson) {
                $this->json(["error" => true, "message" => "Lesson not found"]);
            }

            (new Historic())->create($lesson->id);
            $this->json(["error"=> false, "message" => "Marked success"]);
        } else {
            $this->json(["error" => true, "message" => "Lesson not found"]);
        }
    }
}