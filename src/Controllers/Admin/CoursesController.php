<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Course;
use Src\Models\Module;

class CoursesController extends Controller
{
    protected $course;

    public function __construct()
    {
        $this->auth("admins");
        $this->course = new Course();
    }

    public function index()
    {
        $data = array();
        $data["courses"] = $this->course->all();
        $this->template("admin_course", $data);
    }

    public function create()
    {
        $this->template("admin_course_create");
    }

    public function store()
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $file = array();

        if(isset($_FILES['photo']) && !empty($_FILES["photo"]["tmp_name"])) {
            $file = $_FILES['photo'];
        }

        if (empty($data["name"]) || empty($data["description"])) {
            header("Location: " . BASE_URL . "admin/courses/create?error=fields");
            exit;
        }

        $data["file"] = $file;
        $this->course->add($data);

        header("Location: " . BASE_URL . "admin/courses/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!$course = $this->course->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $module = new \stdClass();
        $module->id = null;
        $module->name = null;

        $data = array();
        $data["course"] = $course;
        $data["modules"] = (new Module())->getByCourse($course->id);
        $data["module"] = $module;
        $this->template("admin_course_edit", $data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $file = array();

        if(isset($_FILES['photo']) && !empty($_FILES["photo"]["tmp_name"])) {
            $file = $_FILES['photo'];
        }

        if (!$this->course->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (empty($data["name"]) || empty($data["description"])) {
            header("Location: " . BASE_URL . "admin/courses/edit/{$id}?error=fields");
            exit;
        }

        $data["file"] = $file;
        $this->course->updateData($data, $id);

        header("Location: " . BASE_URL . "admin/courses/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!$course = $this->course->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $this->course->destroy($course->id);

        header("Location: " . BASE_URL . "admin/courses/index");
        exit;
    }
}