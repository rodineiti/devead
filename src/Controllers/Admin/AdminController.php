<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Admin as User;
use Src\Models\Course;

class AdminController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function index()
    {
        if (auth("admins")) {
            $this->home();
        } else {
            $this->template("admin_login");
        }
    }

    public function home()
    {
        $this->auth("admins");
        $data = array();
        $data["countCourses"] = (new Course())->count(["id"]);
        $data["countStudents"] = (new \Src\Models\User())->count(["id"]);
        $this->template("admin_home", $data);
    }

    public function profile()
    {
        $this->template("admin_profile");
    }

    public function login()
    {
        if(isset($_POST["email"]) && !empty($_POST["email"])) {
            $email = addslashes($_POST["email"]);
            $password = $_POST["password"];

            $user = $this->user->attempt($email, $password);

            if (!$user) {
                header("Location: " . BASE_URL . "admin?login&error=true");
                exit;
            }

            header("Location: " . BASE_URL . "admin/home");
            exit;
        }

        header("Location: " . BASE_URL . "admin?login&error=true");
        exit;
    }

    public function update()
    {
        if($_POST) {
            if (!$this->user->updateProfile(auth("admins")->id, $_POST)) {
                header("Location: " . BASE_URL . "admin/profile?error=fields");
                exit;
            } else {
                header("Location: " . BASE_URL . "admin/profile?success=true");
                exit;
            }
        }

        header("Location: " . BASE_URL . "admin/profile?error=fields");
        exit;
    }

    public function logout()
    {
        session_start();
        unset($_SESSION['userLoggedAdmin']);
        header("Location: " . BASE_URL . "admin?login");
    }
}