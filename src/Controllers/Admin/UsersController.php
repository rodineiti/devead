<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Course;
use Src\Models\StudentCourse;
use Src\Models\User;

class UsersController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->auth("admins");
        $this->user = new User();
    }

    public function index()
    {
        $data = array();
        $data["users"] = $this->user->all();
        $this->template("admin_user", $data);
    }

    public function create()
    {
        $this->template("admin_user_create");
    }

    public function store()
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["name"]) || empty($data["genre"]) || empty($data["email"]) || empty($data["password"])) {
            header("Location: " . BASE_URL . "admin/users/create?error=fields");
            exit;
        }

        $user = $this->user->create($data);

        if (!$user) {
            header("Location: " . BASE_URL . "admin/users/create?error=exists");
            exit;
        }

        header("Location: " . BASE_URL . "admin/users/create?success");
        exit;
    }

    public function edit($id)
    {
        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        $coursesByStudent = (new StudentCourse())->getByStudent($user->id);
        $excludes = parseArray($coursesByStudent, "course_id");
        $courses = (new Course())->all($excludes);

        $data = array();
        $data["user"] = $user;
        $data["courses"] = $courses;
        $data["coursesByStudent"] = $coursesByStudent;
        $this->template("admin_user_edit", $data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        if (empty($data["name"]) || empty($data["genre"]) || empty($data["email"])) {
            header("Location: " . BASE_URL . "admin/users/edit/{$id}?error=fields");
            exit;
        }

        if (!$this->user->updateProfile($id, $data)) {
            header("Location: " . BASE_URL . "admin/users/edit/{$id}?error=fields");
            exit;
        }

        header("Location: " . BASE_URL . "admin/users/edit/{$id}?success=edit");
        exit;
    }

    public function destroy($id)
    {
        if (!$user = $this->user->getById($id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        $this->user->destroy($user->id);

        header("Location: " . BASE_URL . "admin/users/index");
        exit;
    }

    public function add_course($course_id, $user_id)
    {
        if (!$user = $this->user->getById($user_id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        if (!$course = (new Course())->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        $this->user->addCourse($user->id, $course->id);

        header("Location: " . BASE_URL . "admin/users/edit/{$user_id}?success=add");
        exit;
    }

    public function delete_course($course_id, $user_id)
    {
        if (!$user = $this->user->getById($user_id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        if (!$course = (new Course())->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/users/index?error");
            exit;
        }

        $this->user->delCourse($user->id, $course->id);

        header("Location: " . BASE_URL . "admin/users/edit/{$user_id}?success=del");
        exit;
    }
}