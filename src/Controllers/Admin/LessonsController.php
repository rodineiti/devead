<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Course;
use Src\Models\Lesson;
use Src\Models\Module;
use Src\Models\Question;
use Src\Models\Video;

class LessonsController extends Controller
{
    protected $course;
    protected $lesson;

    public function __construct()
    {
        $this->auth("admins");
        $this->course = new Course();
        $this->lesson = new Lesson();
    }

    public function create($module_id, $course_id)
    {
        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($module_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $data = array();
        $data["course"] = $course;
        $data["module"] = $module;
        $this->template("admin_lesson_create", $data);
    }

    public function store_video()
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["name"]) || empty($data["course_id"]) || empty($data["url"]) ||
            empty($data["module_id"]) || empty($data["description"]) || empty($data["order"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$course = $this->course->getById($data["course_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($data["module_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $saveId = $this->lesson->insert([
            "module_id" => $module->id,
            "course_id" => $course->id,
            "type" => "v",
            "`order`" => $data["order"] ?? 0
        ]);

        if ($saveId) {
            (new Video())->insert([
                "lesson_id" => $saveId,
                "name" => $data["name"],
                "description" => $data["description"],
                "url" => $data["url"],
            ]);
        }

        header("Location: " . BASE_URL . "admin/courses/edit/{$course->id}?success=module");
        exit;
    }

    public function store_question()
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["question"]) || empty($data["course_id"]) || empty($data["module_id"]) ||
            empty($data["option1"]) || empty($data["option2"]) || empty($data["option3"]) ||
            empty($data["option4"]) || empty($data["correct"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$course = $this->course->getById($data["course_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($data["module_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $saveId = $this->lesson->insert([
            "module_id" => $module->id,
            "course_id" => $course->id,
            "type" => "q",
            "`order`" => 0
        ]);

        if ($saveId) {
            (new Question())->insert([
                "lesson_id" => $saveId,
                "question" => $data["question"],
                "option1" => $data["option1"],
                "option2" => $data["option2"],
                "option3" => $data["option3"],
                "option4" => $data["option4"],
                "correct" => $data["correct"],
            ]);
        }

        header("Location: " . BASE_URL . "admin/courses/edit/{$course->id}?success=module");
        exit;
    }

    public function edit_lesson($id, $module_id, $course_id)
    {
        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($module_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$lesson = $this->lesson->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $data = array();
        $data["course"] = $course;
        $data["module"] = $module;
        $data["lesson"] = $lesson;
        $this->template("admin_lesson_edit", $data);
    }

    public function update_video($id)
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["name"]) || empty($data["course_id"]) || empty($data["url"]) ||
            empty($data["module_id"]) || empty($data["description"]) || empty($data["order"]) || empty($data["video_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$course = $this->course->getById($data["course_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($data["module_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$lesson = $this->lesson->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$video = (new Video())->getById($data["video_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        (new Video())->update([
            "name" => $data["name"],
            "description" => $data["description"],
            "url" => $data["url"],
        ], ["id" => $video->id, "lesson_id" => $id]);

        header("Location: " . BASE_URL . "admin/courses/edit/{$course->id}?success=module");
        exit;
    }

    public function update_question($id)
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["question"]) || empty($data["course_id"]) || empty($data["module_id"]) ||
            empty($data["option1"]) || empty($data["option2"]) || empty($data["option3"]) ||
            empty($data["option4"]) || empty($data["correct"] || empty($data["question_id"]))) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$course = $this->course->getById($data["course_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($data["module_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$lesson = $this->lesson->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$question = (new Question())->getById($data["question_id"])) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        (new Question())->update([
            "question" => $data["question"],
            "option1" => $data["option1"],
            "option2" => $data["option2"],
            "option3" => $data["option3"],
            "option4" => $data["option4"],
            "correct" => $data["correct"],
        ], ["id" => $question->id, "lesson_id" => $id]);

        header("Location: " . BASE_URL . "admin/courses/edit/{$course->id}?success=module");
        exit;
    }

    public function delete_lesson($id, $module_id, $course_id)
    {
        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = (new Module())->getById($module_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$lesson = $this->lesson->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $this->lesson->destroy($lesson->id);

        header("Location: " . BASE_URL . "admin/courses/edit/{$course->id}?success=module");
        exit;
    }
}