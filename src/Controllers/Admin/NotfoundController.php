<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function index()
    {
        $this->template("404");
    }
}