<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Course;
use Src\Models\Module;

class ModulesController extends Controller
{
    protected $course;
    protected $module;

    public function __construct()
    {
        $this->auth("admins");
        $this->course = new Course();
        $this->module = new Module();
    }

    public function module($course_id)
    {
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (empty($data["name"])) {
            header("Location: " . BASE_URL . "admin/courses/edit/{$course_id}?error=fields");
            exit;
        }

        $data["course_id"] = $course->id;
        if (isset($data["module_id"]) && !empty($data["module_id"])) {
            if (!$module = $this->module->getById($data["module_id"])) {
                header("Location: " . BASE_URL . "admin/courses/index?error");
                exit;
            }
            unset($data["module_id"]);
            $this->module->update($data, ["id" => $module->id]);
        } else {
            unset($data["module_id"]);
            $this->module->insert($data);
        }

        header("Location: " . BASE_URL . "admin/courses/edit/{$course_id}?success=module");
        exit;
    }

    public function edit_module($id, $course_id)
    {
        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = $this->module->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $data = array();
        $data["course"] = $course;
        $data["modules"] = $this->module->getByCourse($course->id);
        $data["module"] = $module;
        $this->template("admin_course_edit", $data);
    }

    public function delete_module($id, $course_id)
    {
        if (!$course = $this->course->getById($course_id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        if (!$module = $this->module->getById($id)) {
            header("Location: " . BASE_URL . "admin/courses/index?error");
            exit;
        }

        $this->module->destroy($module->id);

        header("Location: " . BASE_URL . "admin/courses/edit/{$course_id}?success=module");
        exit;
    }
}