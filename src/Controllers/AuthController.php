<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;

class AuthController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function index()
    {
        $this->template("login");
    }

    public function register()
    {
        $this->template("register");
    }

    public function profile()
    {
        $this->template("profile");
    }

    public function login()
    {
        if(isset($_POST["email"]) && !empty($_POST["email"])) {
            $email = addslashes($_POST["email"]);
            $password = $_POST["password"];

            $user = $this->user->attempt($email, $password);

            if (!$user) {
                header("Location: " . BASE_URL . "auth?login&error=true");
                exit;
            }

            header("Location: " . BASE_URL);
            exit;
        }

        header("Location: " . BASE_URL . "auth?login&error=true");
        exit;
    }

    public function save()
    {
        if(isset($_POST["name"]) && !empty($_POST["name"])) {
            $name = addslashes($_POST["name"]);
            $email = addslashes($_POST["email"]);
            $password = $_POST["password"];
            $genre = addslashes($_POST["genre"]);
            $data["name"] = $name;
            $data["email"] = $email;
            $data["password"] = $password;
            $data["genre"] = $genre;

            if (empty($data["name"]) || empty($data["genre"]) || empty($data["email"]) || empty($data["password"])) {
                header("Location: " . BASE_URL . "auth/register?error=fields");
                exit;
            }

            $user = $this->user->create($data);

            if (!$user) {
                header("Location: " . BASE_URL . "auth/register?error=exists");
                exit;
            }

            header("Location: " . BASE_URL . "auth/register?success=true");
            exit;
        }

        header("Location: " . BASE_URL . "auth/register?error=fields");
        exit;
    }

    public function update()
    {
        if($_POST) {
            $user = $this->user->updateProfile(auth()->id, $_POST);
            if (!$user) {
                header("Location: " . BASE_URL . "auth/profile?error=fields");
                exit;
            } else {
                $_SESSION["userLogged"] = (object)$user;
                header("Location: " . BASE_URL . "auth/profile?success=true");
                exit;
            }
        }

        header("Location: " . BASE_URL . "auth/profile?error=fields");
        exit;
    }

    public function logout()
    {
        session_start();
        unset($_SESSION['userLogged']);
        header("Location: " . BASE_URL);
    }
}