<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Course;
use Src\Models\Doubt;
use Src\Models\Lesson;
use Src\Models\Module;
use Src\Models\User;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->auth();
    }

    public function index($course_id)
    {
        if (!(new User())->isRegisteredCourse($course_id)) {
            header("Location: " . BASE_URL);
            exit;
        }

        $totalWatched = (new User())->getCountWatched();
        $totalLessons = (new Course())->getTotalLessons($course_id);

        $data = array();
        $data["course"] = (new Course())->getById($course_id);
        $data["modules"] = (new Module())->getByCourse($course_id);
        $data["totalWatched"] = $totalWatched;
        $data["totalLessons"] = $totalLessons;
        $this->template("course", $data);
    }

    public function lesson($lesson_id)
    {
        $lesson = (new Lesson())->getById($lesson_id);

        if (!$lesson) {
            header("Location: " . BASE_URL);
            exit;
        }

        $course_id = $lesson->course_id;
        if (!(new User())->isRegisteredCourse($course_id)) {
            header("Location: " . BASE_URL);
            exit;
        }

        $view = "course_lesson";

        if ($lesson->type === "q") {
            $view = "course_question";
            if (!isset($_SESSION["doubt_count_".$lesson->id])) {
                $_SESSION["doubt_count_".$lesson->id] = 1;
            }
            if (isset($_SESSION["doubt_answer_".$lesson->id])) {
                unset($_SESSION["doubt_answer_".$lesson->id]);
            }
        }

        $totalWatched = (new User())->getCountWatched();
        $totalLessons = (new Course())->getTotalLessons($course_id);

        $data = array();
        $data["course"] = (new Course())->getById($course_id);
        $data["modules"] = (new Module())->getByCourse($course_id);
        $data["lesson"] = $lesson;
        $data["totalWatched"] = $totalWatched;
        $data["totalLessons"] = $totalLessons;
        $this->template($view, $data);
    }

    public function save_doubt($lesson_id)
    {
        if ($this->method() !== "POST") {
            header("Location: " . BASE_URL);
            exit;
        }

        $lesson = (new Lesson())->getById($lesson_id);

        if (!$lesson) {
            header("Location: " . BASE_URL);
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["description"])) {
            header("Location: " . BASE_URL . "courses/lesson/" . $lesson_id . "?error=fields");
            exit;
        }

        (new Doubt())->create($data, $lesson->id);
        header("Location: " . BASE_URL . "courses/lesson/" . $lesson->id . "?success=true");
        exit;
    }

    public function send_doubt($lesson_id)
    {
        if ($this->method() !== "POST") {
            header("Location: " . BASE_URL);
            exit;
        }

        $lesson = (new Lesson())->getById($lesson_id);

        if (!$lesson) {
            header("Location: " . BASE_URL);
            exit;
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($data["option"])) {
            header("Location: " . BASE_URL . "courses/lesson/" . $lesson_id . "?error=fields");
            exit;
        }

        if ($_SESSION["doubt_count_".$lesson->id]) {
            $_SESSION["doubt_count_".$lesson->id]++;
        }

        $_SESSION["doubt_answer_".$lesson->id] = ($data["option"] === $lesson->question->correct) ? true : false;

        (new Doubt())->create($data, $lesson->id);
        header("Location: " . BASE_URL . "courses/lesson/" . $lesson->id . "?success=true");
        exit;
    }
}