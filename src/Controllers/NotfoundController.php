<?php

namespace Src\Controllers;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function index()
    {
        $this->template("404");
    }
}