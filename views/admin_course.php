<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/courses/create"; ?>" class="btn btn-primary mb-2">Adicionar</a>
            <?php if (isset($_GET["error"])): ?>
                <div class="alert alert-danger">
                    Opss. Ocorreu um erro no processamento, tente mais tarde.
                </div>
            <?php endif; ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Total de alunos</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($courses as $course): ?>
                    <tr>
                        <th scope="row"><?= $course->id ?></th>
                        <td>
                            <img src="<?= image($course->photo) ?>" height="100" alt="<?= $course->name ?>" title="<?= $course->name ?>" />
                        </td>
                        <td><?= $course->name ?></td>
                        <td><?= $course->countStudents ?></td>
                        <td><?= $course->created_at ?></td>
                        <td>
                            <a href="<?= BASE_URL . "admin/courses/edit/" . $course->id; ?>" class="btn btn-info">Editar</a>
                            <a href="<?= BASE_URL . "admin/courses/destroy/" . $course->id; ?>" class="btn btn-danger">Deletar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>