
<div class="container-fluid">
    <div class="row">
        <?php if (count($studentCourses)): ?>
            <?php foreach($studentCourses as $item): ?>
                <div class="col-md-3">
                    <div class="card">
                        <img src="<?= image($item->photo); ?>" class="card-img-top" alt="<?= $item->name; ?>">
                        <div class="card-body text-center">
                            <h5 class="card-title"><?= $item->name; ?></h5>
                            <p class="card-text"><?= $item->description; ?></p>
                            <a href="<?= BASE_URL . "courses/index/" . $item->course_id; ?>" class="btn btn-primary">Acessar</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="alert alert-info">
                            Você não está matriculado em nenhum curso.
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>