<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <div class="accordion" id="accordionExample">
                <?php if ($lesson->type === "v"): ?>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Vídeo
                            </button>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <a href="<?= BASE_URL . "admin/courses/edit/" . $course->id; ?>" class="btn btn-info mb-2">Voltar</a>
                            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                                <div class="alert alert-warning">
                                    Preencha todos os campos!
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_GET["success"])): ?>
                                <div class="alert alert-success">
                                    <strong>OK!</strong> Criado sucesso.
                                </div>
                            <?php endif; ?>
                            <h1>Editar aula</h1>
                            <form method="POST" action="<?= BASE_URL?>admin/lessons/update_video/<?=$lesson->id?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="name">Nome da aula</label>
                                    <input type="text" name="name" id="name" class="form-control" value="<?=$lesson->video->name?>" required />
                                </div>
                                <div class="form-group">
                                    <label for="url">Link da aula</label>
                                    <input type="text" name="url" id="url" class="form-control" value="<?=$lesson->video->url?>" required />
                                </div>
                                <div class="form-group">
                                    <label for="order">Ordem</label>
                                    <input type="number" name="order" id="order" class="form-control" value="<?=$lesson->order?>" required />
                                </div>
                                <div class="form-group">
                                    <label for="description">Descrição da aula</label>
                                    <textarea name="description" id="description" class="form-control"><?=$lesson->video->description?></textarea>
                                </div>
                                <input type="hidden" name="course_id" value="<?=$course->id?>">
                                <input type="hidden" name="module_id" value="<?=$module->id?>">
                                <input type="hidden" name="video_id" value="<?=$lesson->video->id?>">
                                <input type="submit" value="Atualizar" class="btn btn-primary" />
                            </form>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Questionário
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <a href="<?= BASE_URL . "admin/courses/edit/" . $course->id; ?>" class="btn btn-info mb-2">Voltar</a>
                            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                                <div class="alert alert-warning">
                                    Preencha todos os campos!
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_GET["success"])): ?>
                                <div class="alert alert-success">
                                    <strong>OK!</strong> Criado sucesso.
                                </div>
                            <?php endif; ?>
                            <h1>Editar aula</h1>
                            <form method="POST" action="<?= BASE_URL?>admin/lessons/update_question/<?=$lesson->id?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="question">Nome da questão</label>
                                    <input type="text" name="question" id="question" value="<?=$lesson->question->question?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option1">Opção 1</label>
                                    <input type="text" name="option1" id="option1" value="<?=$lesson->question->option1?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option2">Opção 2</label>
                                    <input type="text" name="option2" id="option2" value="<?=$lesson->question->option2?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option3">Opção 3</label>
                                    <input type="text" name="option3" id="option3" value="<?=$lesson->question->option3?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option4">Opção 4</label>
                                    <input type="text" name="option4" id="option4" value="<?=$lesson->question->option4?>" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="correct">Opção correta</label>
                                    <input type="number" name="correct" id="correct" value="<?=$lesson->question->correct?>" class="form-control" placeholder="1, 2, 3 ou 4" required />
                                </div>
                                <input type="hidden" name="course_id" value="<?=$course->id?>">
                                <input type="hidden" name="module_id" value="<?=$module->id?>">
                                <input type="hidden" name="question_id" value="<?=$lesson->question->id?>">
                                <input type="submit" value="Atualizar" class="btn btn-primary" />
                            </form>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>