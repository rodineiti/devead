
<div class="container-fluid">
    <div class="row mb-2">
        <?=$this->view("header-course", ["course" => $course, "totalWatched" => $totalWatched, "totalLessons" => $totalLessons]);?>
    </div>
    <div class="row">
        <div class="col-md-3 left">
            <?=$this->view("menu", ["modules" => $modules]);?>
        </div>
        <div class="col-md-9 right">
            <?php if (isset($lesson->video)): ?>
            <div class="card">
                <div class="card-body">
                    <h1><?=$lesson->video->name?></h1>
                    <iframe
                            id="frameVideo"
                            src="<?=$lesson->video->url?>"
                            frameborder="0"
                            style="width: 100%;"
                    ></iframe>
                </div>
                <div class="card-footer">
                    <div class="card">
                        <div class="card-body">
                            <?php if (!$lesson->isMarked): ?>
                                <button onclick="markDone(this)" data-id="<?=$lesson->id?>" class="btn btn-info mb-2">Marcar como concluído</button>
                            <?php else: ?>
                                <span class="badge-info p-1">Esta aula já foi assistida.</span>
                            <?php endif; ?>
                            <p class="text-muted mt-2"><?=$lesson->video->description?></p>
                        </div>
                    </div>
                    <div class="card">
                        <?php if (count($lesson->doubts)): ?>
                        <div class="card-header">
                            <h1>Dúvidas e respostas</h1>
                            <ul class="list-group list-group-flush">
                                <?php foreach ($lesson->doubts as $doubt): ?>
                                    <li class="list-group-item">
                                        <div class="h6 text-muted"><?=$doubt->created_at?></div>
                                        <div class="h5"><?=$doubt->description?></div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <div class="card-body">
                            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                                <div class="alert alert-warning">
                                    Preencha o campo abaixo com sua dúvida!
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_GET["success"])): ?>
                                <div class="alert alert-success">
                                    <strong>Obrigado!</strong> Dúvida enviada com sucesso.
                                </div>
                            <?php endif; ?>
                            <form action="<?= BASE_URL . "courses/save_doubt/" . $lesson->id ?>" method="post" enctype="multipart/form-data">
                                <div class="card-header">
                                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="posts-tab"
                                               data-toggle="tab" href="#posts" role="tab" aria-controls="posts"
                                               aria-selected="true">Dúvidas? Envie sua pergunta</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                            <div class="form-group">
                                                <label class="sr-only" for="description">Dúvida</label>
                                                <textarea class="form-control" name="description" id="description" rows="3"
                                                          placeholder="Qual sua dúvida?" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar justify-content-between">
                                        <div class="btn-group">
                                            <input type="hidden" name="lesson_id" id="lesson_id" value="<?=$lesson->id?>" />
                                            <button type="submit" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>