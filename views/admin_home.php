<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">Total de cursos</div>
                        <div class="card-body text-center">
                            <h5 class="card-title"><?= $countCourses; ?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">Total de alunos</div>
                        <div class="card-body text-center">
                            <h5 class="card-title"><?= $countStudents; ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>