
<div class="container-fluid">
    <div class="row mb-2">
        <?=$this->view("header-course", ["course" => $course, "totalWatched" => $totalWatched, "totalLessons" => $totalLessons]);?>
    </div>
    <div class="row">
        <div class="col-md-3 left">
            <?=$this->view("menu", ["modules" => $modules]);?>
        </div>
        <div class="col-md-9 right">
            <div class="card">
                <div class="card-header"><h1>Questionário</h1></div>
                <?php if (isset($lesson->question)): ?>
                <div class="card-body">
                    <h3><?=$lesson->question->question?></h3>
                </div>
                <div class="card-footer">

                    <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                        <div class="alert alert-warning">
                            Preencha o campo abaixo com sua resposta!
                        </div>
                    <?php endif; ?>

                    <?php if ($_SESSION["doubt_count_".$lesson->id] > 2): ?>
                    <div class="alert alert-info">
                        <strong>Que pena:</strong> Você atingiu o limite de tentativas de resposta.
                    </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            <strong>Tentativas:</strong> <?= $_SESSION["doubt_count_".$lesson->id] . " de 2"; ?>
                        </div>

                        <?php if (isset($_SESSION["doubt_answer_".$lesson->id])): ?>
                            <?php if ($_SESSION["doubt_answer_".$lesson->id] === true): ?>
                                <div class="alert alert-success">
                                    <strong>Parabéns!</strong> Você acertou :)
                                </div>
                            <?php else: ?>
                                <div class="alert alert-danger">
                                    <strong>Que pena!</strong> Você error :(
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <form action="<?= BASE_URL . "courses/send_doubt/" . $lesson->id ?>" method="post" enctype="multipart/form-data">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="option" id="option1" value="1">
                                <label class="form-check-label" for="option1">
                                    <?= $lesson->question->option1; ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="option" id="option2" value="2">
                                <label class="form-check-label" for="option2">
                                    <?= $lesson->question->option2; ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="option" id="option3" value="3">
                                <label class="form-check-label" for="option3">
                                    <?= $lesson->question->option3; ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="option" id="option4" value="4">
                                <label class="form-check-label" for="option4">
                                    <?= $lesson->question->option4; ?>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>