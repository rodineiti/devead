<div class="card">
    <div class="card-body">
        <?php foreach ($modules as $module): ?>
            <ul class="list-group list-group-flush">
                <li class="list-group-item active">
                    <a
                            href="<?= BASE_URL . "courses/index/" . $module->course_id; ?>"
                    style="color: #fff;"><?= $module->name; ?></a>
                </li>
                <?php foreach ($module->lessons as $lesson): ?>
                    <li class="list-group-item">
                        <div class="h6 text-muted table-hover d-flex justify-content-between align-items-center">
                            <?php if (isset($_GET["url"]) && $_GET["url"] === "courses/lesson/" . $lesson->id): ?>
                                <img class="mr-2" src="<?= BASE_URL ?>assets/images/arrow.png" height="20" alt="arrow" title="arrow">
                            <?php endif; ?>
                            <a href="<?= BASE_URL . "courses/lesson/" . $lesson->id; ?>">
                                <?= ($lesson->type === "v" ? $lesson->video->name : "Questionário"); ?>
                            </a>
                            <?php if ($lesson->isMarked): ?>
                                <img src="<?= BASE_URL ?>assets/images/check.png" height="20" alt="check" title="check">
                            <?php endif; ?>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>
    </div>
</div>