<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/users/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"]) && $_GET["success"] === "edit"): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Atualizado sucesso.
                </div>
            <?php endif; ?>
            <h1>Editar aluno</h1>
            <form method="POST" action="<?= BASE_URL?>admin/users/update/<?= $user->id; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" value="<?= $user->name?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input type="email" name="email" id="email" value="<?= $user->email?>" class="form-control" required />
                </div>
                <hr>
                <div class="form-group">
                    <label for="password">Senha:</label>
                    <input type="password" name="password" id="password" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="genre1">Sexo:</label>
                    <input type="radio" id="genre1" name="genre" value="male" <?php if ($user->genre == "male"): ?>checked<?php endif; ?> /> Masculino
                    <input type="radio" id="genre2" name="genre" value="female" <?php if ($user->genre == "female"): ?>checked<?php endif; ?> /> Feminino
                    <input type="radio" id="genre3" name="genre" value="other" <?php if ($user->genre == "other"): ?>checked<?php endif; ?> /> Outro
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>

            <div class="row mt-2">
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Cursos</div>
                        <div class="card-body">
                            <?php foreach ($courses as $course): ?>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between">
                                        <?= $course->name; ?>
                                        <a class="btn btn-primary" href="<?= BASE_URL?>admin/users/add_course/<?= $course->id; ?>/<?= $user->id; ?>">
                                            Adicionar curso
                                        </a>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Cursos Matriculados</div>
                        <div class="card-body">
                            <?php foreach ($coursesByStudent as $course): ?>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between">
                                        <?= $course->name; ?>
                                        <a class="btn btn-danger" href="<?= BASE_URL?>admin/users/delete_course/<?= $course->course_id; ?>/<?= $user->id; ?>">
                                            Remover curso
                                        </a>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>