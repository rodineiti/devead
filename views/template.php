<html>
<head>
    <title><?=SITE_NAME?></title>
    <link href="<?= BASE_URL ?>assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="<?= BASE_URL ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light mb-2">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php if (in_array($_GET["url"] ?? "/", check_url()) === false): ?>
                <a href="<?= BASE_URL ?>" class="navbar-brand"><?=SITE_NAME?></a>
            <?php else: ?>
                <a href="<?= BASE_URL . "admin" ?>" class="navbar-brand"><?=SITE_NAME?></a>
            <?php endif; ?>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <?php if (in_array($_GET["url"] ?? "/", check_url()) === false && count(explode("/", $_GET["url"] ?? "/")) < 4): ?>
                <?php if(auth()): ?>
                    <li class="dropdown">
                        <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;"><?=auth()->name?></a>
                        <ul class="dropdown-menu">
                            <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/profile"><?=auth()->name?></a></li>
                            <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/logout">Sair</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth/register">Cadastre-se</a></li>
                    <li><a class="nav-item nav-link" href="<?= BASE_URL ?>auth?login">Login</a></li>
                <?php endif; ?>
            <?php else: ?>
                <?php if(auth("admins")): ?>
                    <li class="dropdown mr-5">
                        <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:;"><?=auth("admins")->name?></a>
                        <ul class="dropdown-menu">
                            <li><a class="nav-item nav-link" href="<?= BASE_URL ?>admin/profile"><?=auth("admins")->name?></a></li>
                            <li><a class="nav-item nav-link" href="<?= BASE_URL ?>admin/logout">Sair</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </div>
</nav>

<?=$this->viewTemplate($view, $data)?>
<script src="<?= BASE_URL ?>assets/js/jquery.min.js"></script>
<script src="<?= BASE_URL ?>assets/js/bootstrap.min.js"></script>
<script>var baseUrl = '<?=BASE_URL?>';</script>
<script src="<?= BASE_URL ?>assets/js/script.js"></script>
</body>
</html>