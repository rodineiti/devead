<div class="card">
    <div class="card-body">
        <ul class="list-group">
            <li class="list-group-item">
                <a href="<?= BASE_URL . "admin/home";?>">
                    Home
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?= BASE_URL . "admin/courses/index";?>">
                    Cursos
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?= BASE_URL . "admin/users/index";?>">
                    Alunos
                </a>
            </li>
        </ul>
    </div>
</div>