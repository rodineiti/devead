<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Vídeo
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <a href="<?= BASE_URL . "admin/courses/edit/" . $course->id; ?>" class="btn btn-info mb-2">Voltar</a>
                            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                                <div class="alert alert-warning">
                                    Preencha todos os campos!
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_GET["success"])): ?>
                                <div class="alert alert-success">
                                    <strong>OK!</strong> Criado sucesso.
                                </div>
                            <?php endif; ?>
                            <h1>Adicionar aula</h1>
                            <form method="POST" action="<?= BASE_URL?>admin/lessons/store_video" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="name">Nome da aula</label>
                                    <input type="text" name="name" id="name" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="url">Link da aula</label>
                                    <input type="text" name="url" id="url" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="order">Ordem</label>
                                    <input type="number" name="order" id="order" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="description">Descrição da aula</label>
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                </div>
                                <input type="hidden" name="course_id" value="<?=$course->id?>">
                                <input type="hidden" name="module_id" value="<?=$module->id?>">
                                <input type="submit" value="Criar" class="btn btn-primary" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Questionário
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <a href="<?= BASE_URL . "admin/courses/edit/" . $course->id; ?>" class="btn btn-info mb-2">Voltar</a>
                            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                                <div class="alert alert-warning">
                                    Preencha todos os campos!
                                </div>
                            <?php endif; ?>
                            <?php if (isset($_GET["success"])): ?>
                                <div class="alert alert-success">
                                    <strong>OK!</strong> Criado sucesso.
                                </div>
                            <?php endif; ?>
                            <h1>Adicionar aula</h1>
                            <form method="POST" action="<?= BASE_URL?>admin/lessons/store_question" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="question">Nome da questão</label>
                                    <input type="text" name="question" id="question" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option1">Opção 1</label>
                                    <input type="text" name="option1" id="option1" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option2">Opção 2</label>
                                    <input type="text" name="option2" id="option2" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option3">Opção 3</label>
                                    <input type="text" name="option3" id="option3" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="option4">Opção 4</label>
                                    <input type="text" name="option4" id="option4" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label for="correct">Opção correta</label>
                                    <input type="number" name="correct" id="correct" class="form-control" placeholder="1, 2, 3 ou 4" required />
                                </div>
                                <input type="hidden" name="course_id" value="<?=$course->id?>">
                                <input type="hidden" name="module_id" value="<?=$module->id?>">
                                <input type="submit" value="Criar" class="btn btn-primary" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>