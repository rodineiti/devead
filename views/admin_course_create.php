<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/courses/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"])): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Criado sucesso.
                </div>
            <?php endif; ?>
            <h1>Adicionar curso</h1>
            <form method="POST" action="<?= BASE_URL?>admin/courses/store" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do curso</label>
                    <input type="text" name="name" id="name" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="description">Descrição do curso</label>
                    <textarea name="description" id="description" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Imagem do curso</label>
                    <input type="file" name="photo" id="photo" class="form-control" />
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>