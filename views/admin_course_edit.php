<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin_menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= BASE_URL . "admin/courses/index"; ?>" class="btn btn-info mb-2">Voltar</a>
            <?php if (isset($_GET["error"]) && $_GET["error"] === "fields"): ?>
                <div class="alert alert-warning">
                    Preencha todos os campos!
                </div>
            <?php endif; ?>
            <?php if (isset($_GET["success"]) && $_GET["success"] === "edit"): ?>
                <div class="alert alert-success">
                    <strong>OK!</strong> Atualizado sucesso.
                </div>
            <?php endif; ?>
            <h1>Editar curso</h1>
            <form method="POST" action="<?= BASE_URL?>admin/courses/update/<?= $course->id; ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do curso</label>
                    <input type="text" name="name" id="name" value="<?= $course->name; ?>" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="description">Descrição do curso</label>
                    <textarea name="description" id="description" class="form-control"><?= $course->description; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Imagem do curso</label>
                    <input type="file" name="photo" id="photo" class="form-control" />
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
            <div class="row">
                <div class="col text-center">
                    <img src="<?= image($course->photo) ?>" alt="foto" class="img-fluid" />
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <div class="card">
                        <div class="card-header">Módulos</div>
                        <div class="card-body">
                            <?php if (isset($_GET["success"]) && $_GET["success"] === "module"): ?>
                                <div class="alert alert-success">
                                    <strong>OK!</strong> Operação realizada sucesso.
                                </div>
                            <?php endif; ?>
                            <div class="card">
                                <div class="card-header">Módulo</div>
                                <div class="card-body">
                                    <form class="form-inline" method="post" action="<?= BASE_URL?>admin/modules/module/<?= $course->id; ?>">
                                        <div class="form-group mx-sm-3 mb-2">
                                            <label for="name_module" class="sr-only">Nome do módulo</label>
                                            <input type="text" class="form-control" name="name" id="name_module"
                                                   value="<?=$module->name?>" placeholder="Nome do módulo" required>
                                            <input type="hidden" name="module_id" value="<?=$module->id?>">
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2">Salvar</button>
                                    </form>
                                </div>
                            </div>
                            <hr>
                            <?php foreach ($modules as $module): ?>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item active d-flex justify-content-between">
                                        <?= $module->name; ?>
                                        <a class="btn btn-info" href="<?= BASE_URL?>admin/lessons/create/<?= $module->id; ?>/<?= $course->id; ?>">
                                            Adicionar nova aula
                                        </a>
                                        <a class="btn btn-warning" href="<?= BASE_URL?>admin/modules/edit_module/<?= $module->id; ?>/<?= $course->id; ?>">
                                            Editar
                                        </a>
                                        <a class="btn btn-danger" href="<?= BASE_URL?>admin/modules/delete_module/<?= $module->id; ?>/<?= $course->id; ?>">
                                            Deletar
                                        </a>
                                    </li>
                                    <?php foreach ($module->lessons as $lesson): ?>
                                        <li class="list-group-item">
                                            <div class="h6 text-muted table-hover d-flex justify-content-between">
                                                <?= ($lesson->type === "v" ? $lesson->video->name : "Questionário"); ?>
                                                <a class="btn btn-info"
                                                   href="<?= BASE_URL?>admin/lessons/edit_lesson/<?= $lesson->id; ?>/<?= $module->id; ?>/<?= $course->id; ?>">
                                                    Editar
                                                </a>
                                                <a class="btn btn-danger"
                                                   href="<?= BASE_URL?>admin/lessons/delete_lesson/<?= $lesson->id; ?>/<?= $module->id; ?>/<?= $course->id; ?>">
                                                    Deletar
                                                </a>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>