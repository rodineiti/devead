
<div class="container-fluid">
    <div class="row mb-2">
        <?=$this->view("header-course", ["course" => $course, "totalWatched" => $totalWatched, "totalLessons" => $totalLessons]);?>
    </div>
    <div class="row">
        <div class="col-md-3 left">
            <?=$this->view("menu", ["modules" => $modules]);?>
        </div>
        <div class="col-md-9 right">
            <div class="card">
                <div class="card-body">
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <h1 class="display-4"><?=$course->name?></h1>
                            <p class="lead"><?=$course->description?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>