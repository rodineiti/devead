<div class="col">
    <div class="card border-0">
        <div class="card-body bg-light">
            <div class="media mb-2">
                <img src="<?= image($course->photo); ?>" class="align-self-center mr-3" alt="logo-course">
                <div class="media-body">
                    <h5 class="mt-0"><?= $course->name; ?></h5>
                    <p><?= $course->description; ?></p>
                    <p><?= $totalWatched; ?> / <?= $totalLessons; ?> (<?=calc_percent($totalWatched, $totalLessons)?>%)</p>
                </div>
            </div>
        </div>
    </div>
</div>