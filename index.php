<?php
session_start();

require "vendor/autoload.php";
require "config.php";
require "helpers.php";

(new Src\Core\Core())->run();