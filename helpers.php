<?php

function is_password($password)
{
    if (password_get_info($password)['algo'] || (mb_strlen($password) >= CONF_PASSWORD_MIN_LEN && mb_strlen($password) <= CONF_PASSWORD_MAX_LEN)) {
        return true;
    }
    return false;
}

function pwd_gen_hash($password)
{
    if (!empty(password_get_info($password)["algo"])) {
        return $password;
    }

    return password_hash($password, CONF_PASSWORD_ALGO, CONF_PASSWORD_OPTION);
}

function pwd_verify($password, $hash)
{
    return password_verify($password, $hash);
}

function pwd_rehash($hash)
{
    return password_needs_rehash($hash, CONF_PASSWORD_ALGO, CONF_PASSWORD_OPTION);
}

function parseArray($data, $field)
{
    $arr = array();
    foreach ($data as $item) {
        $arr[] = $item->$field;
    }
    return $arr;
}

function auth($guard = "users")
{
    switch ($guard) {
        case "admins":
            return isset($_SESSION["userLoggedAdmin"]) ? $_SESSION["userLoggedAdmin"] : null;
        default:
            return isset($_SESSION["userLogged"]) ? $_SESSION["userLogged"] : null;
    }
}

function image($image = null)
{
    if ($image) {
        return BASE_URL . "media/photos/{$image}";
    }
    return null;
}

function dd(...$value)
{
    print("<pre>".print_r($value,true)."</pre>");
    die;
}

function check_url()
{
    return [
        "admin",
        "admin/home",
        "admin/logout",
        "admin/login",
        "admin/courses/index",
        "admin/courses/create",
        "admin/courses/store",
        "admin/courses/edit",
        "admin/courses/update",
        "admin/courses/destroy",
        "admin/lessons/create",
        "admin/lessons/store_video",
        "admin/lessons/store_question",
        "admin/users/index",
        "admin/users/create",
        "admin/users/store",
        "admin/users/edit",
        "admin/users/update",
        "admin/users/destroy",
        "admin/users/add_course",
        "admin/users/delete_course",
        "admin/profile",
        "admin/update",
    ];
}

function calc_percent($a, $b)
{
    return (($a / $b) * 100);
}
