function updateLeft() {
    // var heightOriginal = $(document.body).height();
    // var posy = $(".left").offset().top;
    // var height = heightOriginal - posy;
    // $(".left, .right").css("height", height + "px");

    var rate = 1920/1080;
    var video = $("#frameVideo");
    var videoWidth = video.width();
    var videoHeight = videoWidth / rate;
    video.css("height", videoHeight + "px");
}
setInterval(updateLeft, 500);

function markDone(obj) {
    var lesson_id = $(obj).attr("data-id");
    if (lesson_id) {
        $.ajax({
            type: "POST",
            url: baseUrl + "ajax/mark_done",
            data: {lesson_id:lesson_id},
            success: function ($data) {
                $(obj).remove();
            }
        });
    }
}
