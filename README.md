# devead

Clone the repository

    git clone git@gitlab.com:rodineiti/devead.git

Switch to the repo folder

    cd devead
    
Edit file config.php, and set connection mysql

    $config["dbname"] = "devead";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";

Dump file devead.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost:2020
  
    php -S localhost:2020

Urls:

    User login: http://localhost:2020/devead/auth?login
    User register: http://localhost:2020/devead/auth/register
    Admin login: http://localhost:2020/devead/admin?login

Prints:

Login:
![image](https://user-images.githubusercontent.com/25492122/88341985-f021ac00-cd14-11ea-9b36-198f8aa5cbc5.png)

Register:
![image](https://user-images.githubusercontent.com/25492122/88342029-0465a900-cd15-11ea-8a7b-5657ee1f331d.png)

Home User:
![image](https://user-images.githubusercontent.com/25492122/88342064-18110f80-cd15-11ea-914d-ca7cd94cb0a7.png)

Course User:
![image](https://user-images.githubusercontent.com/25492122/88342080-23643b00-cd15-11ea-9579-db0b2d2ec463.png)

Lesson current:
![image](https://user-images.githubusercontent.com/25492122/88342219-64f4e600-cd15-11ea-9488-e1b9b1de4be6.png)

Questions:
![image](https://user-images.githubusercontent.com/25492122/88343492-e2b9f100-cd17-11ea-8e0f-508d7002c9d9.png)

Doubts:
![image](https://user-images.githubusercontent.com/25492122/88342278-7b02a680-cd15-11ea-8d18-0a19fa2d5dbb.png)

Profile User and Admin:
![image](https://user-images.githubusercontent.com/25492122/88342343-953c8480-cd15-11ea-880d-0f7fe403b541.png)

Login Admin:
![image](https://user-images.githubusercontent.com/25492122/88342372-a38aa080-cd15-11ea-85bb-68ffacf226b9.png)

Dashboard Admin:
![image](https://user-images.githubusercontent.com/25492122/88342391-b1402600-cd15-11ea-806f-76955b2aa74f.png)

Crud Courses, modules and lessons/question:
![image](https://user-images.githubusercontent.com/25492122/88342430-c74de680-cd15-11ea-99ad-5981342cf134.png)

![image](https://user-images.githubusercontent.com/25492122/88342467-d6349900-cd15-11ea-8441-9a26454266ed.png)

![image](https://user-images.githubusercontent.com/25492122/88343286-750dc500-cd17-11ea-8406-1baf671f9843.png)

Crud Users
![image](https://user-images.githubusercontent.com/25492122/88342496-e3ea1e80-cd15-11ea-9a12-21cbeb52e2e2.png)

![image](https://user-images.githubusercontent.com/25492122/88342546-f7958500-cd15-11ea-8b74-b120441c67a8.png)
